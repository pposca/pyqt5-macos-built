import glob
import os
from distutils.core import setup

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README'), encoding='utf-8') as f:
    long_description = f.read()


def get_package_data():
    package_data = [glob.glob(dir + '/*') for dir, _, _ in os.walk('PyQt5')]
    package_data = [f for sublist in package_data for f in sublist
                    if not os.path.isdir(f)]
    package_data = [path.replace('PyQt5/', '') for path in package_data]

    return package_data

setup(
    name='pyqt5-macos-built',
    version='5.5.0',
    description='This package installs a prebuilt version of PyQt5 for Mac OS',
    long_description=long_description,
    url='https://bitbucket.org/pposca/pyqt5-macos-built',
    bugtrack_url='https://bitbucket.org/pposca/pyqt5-macos-built/issues',
    author='Pepe Osca',
    author_email='',
    license='GPL',
    packages=['PyQt5'],
    install_requires=['docopt'],
    zip_safe=False,
    scripts=['bin/pyqt5_macos_built.py'],
    package_data={
        'PyQt5': get_package_data(),
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications :: Qt',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: MacOS',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries :: Application Frameworks'
    ],
    keywords='qt5 pyqt5 macos',
)
